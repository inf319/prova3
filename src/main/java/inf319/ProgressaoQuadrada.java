package inf319;

public class ProgressaoQuadrada extends Progressao {
	private Progressao progressao;
	private final int EXPOENTE = 2;
	
	public ProgressaoQuadrada(Progressao p) {
		this.progressao = p;
		inicia();
	}

	@Override
	public long inicia() {
		return (long) Math.pow(this.progressao.inicia(), EXPOENTE);
	}

	@Override
	public long proxTermo() {
		return (long) Math.pow(this.progressao.proxTermo(), EXPOENTE);
	}

}
