package inf319;

public class ProgressaoGeometrica extends Progressao {
    
    private long base;

    public ProgressaoGeometrica() {
        this(2);
    }

    public ProgressaoGeometrica(int base) {
        this.base = base;
        inicia();
    }

    public long inicia() {
        valCor = 1;
        return valCor;
    }

    public long proxTermo() {
        valCor *= base;
        return valCor;
    }

}
