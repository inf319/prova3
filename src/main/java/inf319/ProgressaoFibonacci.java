package inf319;

public class ProgressaoFibonacci extends Progressao {
    
    private long valPrev;

    public ProgressaoFibonacci() {
        inicia();
    }
    
    public long inicia() {
	valCor  = 0;
	valPrev = 1;
	return valCor;
    }

    public long proxTermo() {
        valCor += valPrev;
        valPrev = valCor - valPrev;
	return valCor;
    }
}
